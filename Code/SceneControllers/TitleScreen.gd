extends Node2D

var initialLevel = ResourceLoader.load("res://Scenes/Levels/Initial.tscn") as PackedScene

func _input(event):
	if event is InputEventKey:
		var keyEvent = event as InputEventKey
		if (keyEvent.pressed && ((keyEvent.scancode >= KEY_A && keyEvent.scancode <= KEY_K) || keyEvent.scancode == KEY_ENTER)):
			var _unusedValue = get_tree().change_scene_to(initialLevel)
			
	if event is InputEventJoypadButton:
		var buttonEvent = event as InputEventJoypadButton
		if buttonEvent.pressed:
			var _unusedValue = get_tree().change_scene_to(initialLevel)

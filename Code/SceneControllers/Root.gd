extends Node

var splashScreen = ResourceLoader.load("res://Scenes/SplashScreen.tscn") as PackedScene

func _ready():
	var _unusedValue = get_tree().change_scene_to(splashScreen)

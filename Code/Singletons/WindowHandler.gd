extends Node

var screenSize = OS.get_screen_size()
var windowSize = OS.window_size

func _ready():
	# Center window
	var centeredWindow = Vector2((screenSize.x*0.5 - windowSize.x*0.5),(screenSize.y*0.5 - windowSize.y*0.5))
	OS.window_position = centeredWindow
	
func _process(_delta):
	if Input.is_action_just_pressed("window_toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen;
	elif Input.is_action_just_pressed("window_exit_game"):
		get_tree().quit()

# Godot Project

A simple Godot project with a splash screen, a title screen, a folder hierarchy, and a continuous release pipeline to itch.io.

### Overview

The recommended usage is to create a branch called "dev" then lock the "master" branch to only accept merge requests (The second part is recommended as the project is released with every successful build on the "master" branch). The "dev" branch should be used for daily work. Then, when the project is ready for a new release to the experimental project (see experimental builds requirements below), change the version number in version.txt, and it will be released if the pipeline is successful (ex. for betas).

### Quickfacts

Currently only uses Godot 3.3 Mono (the GODOT_VERSION variable doesn't do anything yet)

The pipeline releases to itch.io for every successful build on the "master" branch.

The pipeline is also configured to release to an experimental builds project (could be used for betas, for example) on the "dev" branch for every successful build with a modified version.txt (this file is used as the version number).

### Pipeline Requirements!

For the pipeline to work, the following three variables must be set in GitLab's CI settings.
```
   BUTLER_API_KEY  = secret_key_here   # An itch.io token attached to the account of the project owner or admin (butler is a script used for deployment)
   ITCHIO_GAME     = godot-project     # The name of the game as it appears in the itch.io url (ex. https://coffeef0x.itch.io/godot-project-template)
   ITCHIO_USERNAME = CoffeeF0x         # The username of the itch.io project owner or admin (whose api key is being used) 
```

API tokens can be generated here: https://itch.io/user/settings/api-keys

It is also _STRONGLY_ recommended that the api token be masked and protected.

### Experimental Builds Requirements

For the experimental builds to work, an itch.io project with the same link appended with '-exp' must be created. You must also be using a branch called "dev"

For example, if your project is named "Godot Project Template" and the link is https://coffeef0x.itch.io/godot-project-template, create a new project named "Godot Project Template Exp" (The name doesn't really matter, the link is what's important) where the link will be https://coffeef0x.itch.io/godot-project-template-exp. (btw, these two links actually work, see for yourself)

### Disabling Experimental Builds Deployment

To disable the experimental build deployments (designed to be used for beta/alpha deployments), simply change the variable ALLOW_EXPERIMENTAL_RELEASE from "true" to "false"

### Renaming The "dev" Branch

The "dev" branch name can be changed using the EXPERIMENTAL_BUILDS_BRANCH variable near the top of the .gitlab-ci.yml file (This must match the branch that you want experimental builds to release from)

### Renaming The Project

From Godot's project manager select 'Rename' (this will rename several files and generate new hashes for the project automatically)

(Be sure to close the project if you have it opened)

Then from the 'Godot Project.csproj' file copy the following to the new csproj file (duplicate lines aren't necessary)
```
    <Compile Include="code\sceneControllers\Root.cs" />
    <Compile Include="code\sceneControllers\SplashScreen.cs" />
    <Compile Include="code\sceneControllers\TitleScreen.cs" />
    <Compile Include="code\singletons\WindowHandler.cs" />
    <Compile Include="Properties\AssemblyInfo.cs" />
```

And finally, delete the 'Godot Project.csproj' and 'Godot Project.sln' files
